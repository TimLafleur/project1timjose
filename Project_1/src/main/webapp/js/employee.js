(function(){

    const showRemibursements = (evt) => {
        evt.preventDefault();
        document.getElementById('viewReimbursements').classList.remove('hidden');
        document.getElementById('submitReimbursement').classList.add('hidden');
        document.getElementById('viewMyInformation').classList.add('hidden');
        getReimbursements();
    };

    const showReimbursementSubmit = (evt) => {
        evt.preventDefault();
        document.getElementById('submitReimbursement').classList.remove('hidden');
        document.getElementById('viewReimbursements').classList.add('hidden');
        document.getElementById('viewMyInformation').classList.add('hidden');
    };
	
	const showMyInformation = (evt) => {
        evt.preventDefault();
        document.getElementById('viewMyInformation').classList.remove('hidden');
        document.getElementById('viewReimbursements').classList.add('hidden');
		document.getElementById('submitReimbursement').classList.add('hidden');
    };


    function getReimbursements(){
        // CREATES A NEW URL WITH THE PARAMETERS YOU NEED TO PASS
        var url = new URL('http://localhost:8080/reimbursements'),
            params = {action: 'getReimbursementsWhere'};
        // MAPS THE PARAMETERS FROM THE LIST ABOVE
        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
        // FETCH CALL
        fetch(url, {
            method: 'GET',
        }) .then(resp => {

            resp.json().then(function(data) {
            console.log(data);
            let table = document.getElementById("reimbursementsTable");
            while(table.firstChild){
                table.removeChild(table.firstChild);
            }
            let tr1 = document.createElement('tr');
            let th = document.createElement('th');
            th.innerText = 'Reimbursement Number';
            tr1.appendChild(th);
            let th1 = document.createElement('th');
            th1.innerText = 'Amount';
            tr1.appendChild(th1);
            let th2 = document.createElement('th');
            th2.innerText = 'Status';
            tr1.appendChild(th2);
            let th3 = document.createElement('th');
            th3.innerText = 'Employee';
            tr1.appendChild(th3);
            table.appendChild(tr1);
            for (let i = 0; i < data.length; i++){
                let tr = document.createElement('tr');
                let reimNum = document.createElement('td');
                let amount = document.createElement('td');
                let status = document.createElement('td');
                let employee = document.createElement('td');
                let manager = document.createElement('td');
                reimNum.innerText = data[i]['reimbursementNumber'];
                tr.appendChild(reimNum);
                amount.innerText = data[i]['amount'];
                tr.appendChild(amount);
                status.innerText = data[i]['status'];
                tr.appendChild(status);
                employee.innerText = data[i]['employee_id'];
                tr.appendChild(employee);

                table.appendChild(tr);
            }
            // USE 'DATA' TO RETRIEVE INFORMATION
        })

    })
    .catch(resp => {
            /*if (resp.status == 500) location.href = "http://localhost:8080/index.html"*/
        });
    }

    const logout = (evt) => {
        evt.preventDefault();
        // CREATES A NEW URL WITH THE PARAMETERS YOU NEED TO PASS
        console.log("here we are logout")

        var url = new URL('http://localhost:8080/login'),
            params = {action: 'logout'};
        // MAPS THE PARAMETERS FROM THE LIST ABOVE
        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
        // FETCH CALL
        fetch(url, {
            method: 'GET',
        }) .then(resp => {
            location.href = "http://localhost:8080/login.html";
            resp.json().then(function(data) {

            // USE 'DATA' TO RETRIEVE INFORMATION
        })

    })
    .catch(resp => {
            /*if (resp.status == 500) location.href = "http://localhost:8080/index.html"*/
        });
    }

    const submitNewEmployeeInfo = (evt) => {
        evt.preventDefault();
        // CREATES A NEW URL WITH THE PARAMETERS YOU NEED TO PASS
        console.log("here we are")
        var firstName = document.getElementById('firstName').value;
        var lastName = document.getElementById('lastName').value;
        var email = document.getElementById('email').value;
        var password = document.getElementById('password').value;
        var url = new URL('http://localhost:8080/employees'),
            params = {action: 'updateEmployeeInfo', firstName: firstName,
                lastName: lastName, email: email, password: password};
        // MAPS THE PARAMETERS FROM THE LIST ABOVE
        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
        // FETCH CALL
        fetch(url, {
            method: 'GET',
        }) .then(resp => {

            resp.json().then(function(data) {

            // USE 'DATA' TO RETRIEVE INFORMATION
        })

    })
    .catch(resp => {
            /*if (resp.status == 500) location.href = "http://localhost:8080/index.html"*/
        });
    }
    const submitReimbursement = (evt) => {
        evt.preventDefault();
        // CREATES A NEW URL WITH THE PARAMETERS YOU NEED TO PASS
        console.log("here we are")
        var amount = document.getElementById('amount').value;
        var url = new URL('http://localhost:8080/reimbursements'),
            params = {action: 'insertReimbursement', amount: amount};
        // MAPS THE PARAMETERS FROM THE LIST ABOVE
        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
        // FETCH CALL
        fetch(url, {
            method: 'GET',
        }) .then(resp => {

            resp.json().then(function(data) {

                // USE 'DATA' TO RETRIEVE INFORMATION
            })

    })
    .catch(resp => {
            /*if (resp.status == 500) location.href = "http://localhost:8080/index.html"*/
    });
    }

    document.getElementById('logout')
        .addEventListener('click', logout);
    document.getElementById('saveInfo')
        .addEventListener('click', submitNewEmployeeInfo);
    document.getElementById('viewReimbursementsOption')
        .addEventListener('click', showRemibursements);
    document.getElementById('submitReimbursmentOption')
        .addEventListener('click', showReimbursementSubmit);
    document.getElementById('reimbursement-submit')
        .addEventListener('click', submitReimbursement);
	document.getElementById('viewInfo')
        .addEventListener('click', showMyInformation);
})()
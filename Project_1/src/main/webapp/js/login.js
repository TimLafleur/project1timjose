(function(){
    const doLogin = (evt) => {
        evt.preventDefault();
        const username = document.querySelector('#username').value;
        const password = document.querySelector('#password').value;

        fetch("http://localhost:8080/login", {
            method: 'POST',
            body: JSON.stringify({userName: username,  userPwd: password})
        })
            .then(res => {
                if(res.ok) {
                    return res.json();
                } else {
                    if(res.status === 401 || res.status === 400) {
                        document.querySelector('#error').classList.remove('hidden');
                        throw {};
                    }
                }
            })
            .then(token => {
                localStorage.setItem('username', token.employee);
                localStorage.setItem('type', token.employeeType);
                console.log(token.employeeType);
                if(token.employeeType === 'manager'){
                    console.log(token.employeeType);
                    location.href = "http://localhost:8080/admin_profile.html"
                } else {
                    location.href = "http://localhost:8080/employee_menu.html"
                }

            })
            .catch(error => {});
    };

    document.querySelector('form')
        .addEventListener('submit', doLogin);
    document.querySelector('#login-submit')
        .addEventListener('click', doLogin);
})()
(function(){

    const showEmployeesTable = (evt) => {
        evt.preventDefault();
        document.getElementById('employeesSection').classList.remove('hidden');
        document.getElementById('reimbursements').classList.add('hidden');
        document.getElementById('selectedEmployeeSection').classList.add('hidden');
        getEmployees();
    };

    const showReimbursements = (evt) => {
        evt.preventDefault();
        document.getElementById('employeesSection').classList.add('hidden');
        document.getElementById('reimbursements').classList.remove('hidden');
        document.getElementById('selectedEmployeeSection').classList.add('hidden');
        getReimbursements();
    };

    const showMyInformation = (evt) => {
        evt.preventDefault();
        document.getElementById('viewMyInformation').classList.remove('hidden');
        document.getElementById('viewReimbursements').classList.add('hidden');
        document.getElementById('submitReimbursement').classList.add('hidden');
        document.getElementById('selectedEmployeeSection').classList.add('hidden');
    };

    const showSelectedEmployee = () => {
        document.getElementById('selectedEmployeeSection').classList.remove('hidden');
        document.getElementById('employeesSection').classList.add('hidden');
        document.getElementById('reimbursements').classList.add('hidden');
        //getReimbursements();
    };


    function getEmployees(){
        // CREATES A NEW URL WITH THE PARAMETERS YOU NEED TO PASS
        var url = new URL('http://localhost:8080/employees'),
            params = {action: 'getAllEmployees'};
        // MAPS THE PARAMETERS FROM THE LIST ABOVE
        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
        // FETCH CALL
        fetch(url, {
            method: 'GET',
        }) .then(resp => {

            resp.json().then(function(data) {
            console.log(data);
            let table = document.getElementById("employeeTable");
            while(table.firstChild){
                table.removeChild(table.firstChild);
            }
            let tr1 = document.createElement('tr');
            let th = document.createElement('th');
            th.innerText = 'First Name';
            tr1.appendChild(th);
            let th1 = document.createElement('th');
            th1.innerText = 'Last Name';
            tr1.appendChild(th1);
            let th2 = document.createElement('th');
            th2.innerText = 'Employee Number';
            tr1.appendChild(th2);
            let th3 = document.createElement('th');
            th3.innerText = 'Email';
            tr1.appendChild(th3);
            table.appendChild(tr1);
            for (let i = 0; i < data.length; i++){
                let tr = document.createElement('tr');
                let firstName = document.createElement('td');
                let lastName = document.createElement('td');
                let employeeNum = document.createElement('td');
                let userName = document.createElement('td');
                firstName.innerText = data[i]['firstName'];
                tr.appendChild(firstName);
                lastName.innerText = data[i]['lastName'];
                tr.appendChild(lastName);
                employeeNum.innerText = data[i]['employeeNumber'];
                tr.appendChild(employeeNum);
                userName.innerText = data[i]['email'];
                tr.appendChild(userName);

                table.appendChild(tr);
            }
            // USE 'DATA' TO RETRIEVE INFORMATION
        })

    })
    .catch(resp => {
            /*if (resp.status == 500) location.href = "http://localhost:8080/index.html"*/
        });
    }

    const submitNewEmployeeInfo = (evt) => {
        evt.preventDefault();
        // CREATES A NEW URL WITH THE PARAMETERS YOU NEED TO PASS
        console.log("here we are");
        var firstName = document.getElementById('firstName').value;
        var lastName = document.getElementById('lastName').value;
        var email = document.getElementById('email').value;
        var password = document.getElementById('password').value;
        var url = new URL('http://localhost:8080/employees'),
            params = {action: 'updateEmployeeInfo', firstName: firstName,
                lastName: lastName, email: email, password: password};
        // MAPS THE PARAMETERS FROM THE LIST ABOVE
        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
        // FETCH CALL
        fetch(url, {
            method: 'GET',
        }) .then(resp => {

            resp.json().then(function(data) {

            // USE 'DATA' TO RETRIEVE INFORMATION
        })

    })
    .catch(resp => {
            /*if (resp.status == 500) location.href = "http://localhost:8080/index.html"*/
        });
    }

    function getReimbursementsForSelected(){
        // CREATES A NEW URL WITH THE PARAMETERS YOU NEED TO PASS

        var url = new URL('http://localhost:8080/reimbursements'),
            params = {action: 'getReimbursementsWhereManagerSelected'};
        // MAPS THE PARAMETERS FROM THE LIST ABOVE
        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
        // FETCH CALL
        fetch(url, {
            method: 'GET',
        }) .then(resp => {

            resp.json().then(function(data) {
            console.log(data);
            let table = document.getElementById("reimbursementsTable");
            while(table.firstChild){
                table.removeChild(table.firstChild);
            }
            let tr1 = document.createElement('tr');
            let th = document.createElement('th');
            th.innerText = 'Reimbursement Number';
            tr1.appendChild(th);
            let th1 = document.createElement('th');
            th1.innerText = 'Amount';
            tr1.appendChild(th1);
            let th2 = document.createElement('th');
            th2.innerText = 'Status';
            tr1.appendChild(th2);
            let th3 = document.createElement('th');
            th3.innerText = 'Employee';
            tr1.appendChild(th3);
            table.appendChild(tr1);
            for (let i = 0; i < data.length; i++){
                let tr = document.createElement('tr');
                let reimNum = document.createElement('td');
                let amount = document.createElement('td');
                let status = document.createElement('td');
                let employee = document.createElement('td');
                let manager = document.createElement('td');
                reimNum.innerText = data[i]['reimbursementNumber'];
                tr.appendChild(reimNum);
                amount.innerText = data[i]['amount'];
                tr.appendChild(amount);
                status.innerText = data[i]['status'];
                tr.appendChild(status);
                employee.innerText = data[i]['employee_id'];
                tr.appendChild(employee);

                table.appendChild(tr);
            }
            document.getElementById('reimbursements').classList.remove('hidden');
            // USE 'DATA' TO RETRIEVE INFORMATION
        })

    })
    .catch(resp => {
            /*if (resp.status == 500) location.href = "http://localhost:8080/index.html"*/
        });
    }



    const selectEmployee = (evt) => {
        evt.preventDefault();
        showSelectedEmployee();
        // CREATES A NEW URL WITH THE PARAMETERS YOU NEED TO PASS
        let employeeNumber = document.getElementById("employee").value;
        console.log("here we are")
        var url = new URL('http://localhost:8080/employees'),
            params = {action: 'findOneEmployee', employeeId: employeeNumber};
        // MAPS THE PARAMETERS FROM THE LIST ABOVE
        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
        // FETCH CALL
        fetch(url, {
            method: 'GET',
        }) .then(resp => {
            resp.json().then(function(data) {
            let table = document.getElementById("selectedEmployeeTable");
            while (table.firstChild) {
                table.removeChild(table.firstChild);
            }
            let tr1 = document.createElement('tr');
            let th = document.createElement('th');
            th.innerText = 'First Name';
            tr1.appendChild(th);
            let th1 = document.createElement('th');
            th1.innerText = 'Last Name';
            tr1.appendChild(th1);
            let th2 = document.createElement('th');
            th2.innerText = 'Employee Number';
            tr1.appendChild(th2);
            let th3 = document.createElement('th');
            th3.innerText = 'Email';
            tr1.appendChild(th3);
            table.appendChild(tr1);
            let tr = document.createElement('tr');
            let firstName = document.createElement('td');
            let lastName = document.createElement('td');
            let employeeNum = document.createElement('td');
            let userName = document.createElement('td');
            firstName.innerText = data['firstName'];
            tr.appendChild(firstName);
            lastName.innerText = data['lastName'];
            tr.appendChild(lastName);
            employeeNum.innerText = data['employeeNumber'];
            tr.appendChild(employeeNum);
            userName.innerText = data['email'];
            tr.appendChild(userName);

            table.appendChild(tr);

            getReimbursementsForSelected();
        })
            // USE 'DATA' TO RETRIEVE INFORMATION
    })
    .catch(resp => {
            /*if (resp.status == 500) location.href = "http://localhost:8080/index.html"*/
        });
    }

    const updateReimbursementStatus = (evt, id, statusChange) => {
        evt.preventDefault();
        // CREATES A NEW URL WITH THE PARAMETERS YOU NEED TO PASS
        console.log("here we are")
        var url = new URL('http://localhost:8080/reimbursements'),
            params = {action: 'updateStatus', reimbursementNumber: id, statusChange: statusChange};
        // MAPS THE PARAMETERS FROM THE LIST ABOVE
        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
        // FETCH CALL
        fetch(url, {
            method: 'GET',
        }) .then(resp => {

            resp.json().then(function(data) {

            // USE 'DATA' TO RETRIEVE INFORMATION
        })

    })
    .catch(resp => {
            /*if (resp.status == 500) location.href = "http://localhost:8080/index.html"*/
        });
    }
    function getReimbursements(){
        // CREATES A NEW URL WITH THE PARAMETERS YOU NEED TO PASS
        var url = new URL('http://localhost:8080/reimbursements'),
            params = {action: 'getReimbursements'};
        // MAPS THE PARAMETERS FROM THE LIST ABOVE
        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
        // FETCH CALL
        fetch(url, {
            method: 'GET',
        }) .then(resp => {

            resp.json().then(function(data) {
            console.log(data);
            let table = document.getElementById("reimbursementsTable");
            while(table.firstChild){
                table.removeChild(table.firstChild);
            }
            let tr1 = document.createElement('tr');
            let th = document.createElement('th');
            th.innerText = 'Reimbursement Number';
            tr1.appendChild(th);
            let th1 = document.createElement('th');
            th1.innerText = 'Amount';
            tr1.appendChild(th1);
            let th2 = document.createElement('th');
            th2.innerText = 'Status';
            tr1.appendChild(th2);
            let th3 = document.createElement('th');
            th3.innerText = 'Employee';
            tr1.appendChild(th3);
            table.appendChild(tr1);
            for (let i = 0; i < data.length; i++){
                let tr = document.createElement('tr');
                let reimNum = document.createElement('td');
                let amount = document.createElement('td');
                let status = document.createElement('td');
                let employee = document.createElement('td');
                let manager = document.createElement('td');
                reimNum.innerText = data[i]['reimbursementNumber'];
                tr.appendChild(reimNum);
                amount.innerText = data[i]['amount'];
                tr.appendChild(amount);
                status.innerText = data[i]['status'];
                tr.appendChild(status);
                employee.innerText = data[i]['employee_id'];
                tr.appendChild(employee);
/*                if(data[i]['status'] === 'pending'){
                    let btn1 = document.createElement('button');
                    btn1.innerText = 'Approve';
                    btn1.addEventListener('click',updateReimbursementStatus(data[i]['status'], 'approve'));
                    let btn2 = document.createElement('button');
                    btn2.innerText = 'Deny';
                    btn2.addEventListener('click',updateReimbursementStatus(data[i]['status'], 'deny'));
                    tr.appendChild(btn1);
                    tr.appendChild(btn2);
                }*/

                table.appendChild(tr);
            }
            // USE 'DATA' TO RETRIEVE INFORMATION
        })

    })
    .catch(resp => {
            /*if (resp.status == 500) location.href = "http://localhost:8080/index.html"*/
        });
    }

    const approveReimbursement = (evt) => {
        evt.preventDefault();
        // CREATES A NEW URL WITH THE PARAMETERS YOU NEED TO PASS
        console.log("here we are")
        var reimbursementNumber = document.getElementById('reimSearch').value;
        var url = new URL('http://localhost:8080/reimbursements'),
            params = {action: 'approveReimbursement', reimbursement: reimbursementNumber};
        // MAPS THE PARAMETERS FROM THE LIST ABOVE
        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
        // FETCH CALL
        fetch(url, {
            method: 'GET',
        }) .then(resp => {

            resp.json().then(function(data) {

            // USE 'DATA' TO RETRIEVE INFORMATION
        })

    })
    .catch(resp => {
            /*if (resp.status == 500) location.href = "http://localhost:8080/index.html"*/
        });
    }

    const denyReimbursement = (evt) => {
        evt.preventDefault();
        // CREATES A NEW URL WITH THE PARAMETERS YOU NEED TO PASS
        console.log("here we are")
        var reimbursementNumber = document.getElementById('reimSearch').value;
        var url = new URL('http://localhost:8080/reimbursements'),
            params = {action: 'denyReimbursement', reimbursement: reimbursementNumber};
        // MAPS THE PARAMETERS FROM THE LIST ABOVE
        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
        // FETCH CALL
        fetch(url, {
            method: 'GET',
        }) .then(resp => {

            resp.json().then(function(data) {

            // USE 'DATA' TO RETRIEVE INFORMATION
        })

    })
    .catch(resp => {
            /*if (resp.status == 500) location.href = "http://localhost:8080/index.html"*/
        });
    }

    const logout = (evt) => {
        evt.preventDefault();
        // CREATES A NEW URL WITH THE PARAMETERS YOU NEED TO PASS
        console.log("here we are logout")

        var url = new URL('http://localhost:8080/login'),
            params = {action: 'logout'};
        // MAPS THE PARAMETERS FROM THE LIST ABOVE
        Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));
        // FETCH CALL
        fetch(url, {
            method: 'GET',
        }) .then(resp => {
            location.href = "http://localhost:8080/login.html";
            resp.json().then(function(data) {

            // USE 'DATA' TO RETRIEVE INFORMATION
        })

    })
    .catch(resp => {
            /*if (resp.status == 500) location.href = "http://localhost:8080/index.html"*/
        });
    }


    document.getElementById('selectEmployee')
        .addEventListener('click', selectEmployee);
    document.getElementById('logout')
        .addEventListener('click', logout);
    document.getElementById('viewEmployees')
        .addEventListener('click', showEmployeesTable);
    document.getElementById('viewReimbursements')
        .addEventListener('click', showReimbursements);
    document.getElementById('approved')
        .addEventListener('click', approveReimbursement);
    document.getElementById('denied')
        .addEventListener('click', denyReimbursement);
/*    document.getElementById('saveInfo')
        .addEventListener('click', submitNewEmployeeInfo);
    document.getElementById('viewReimbursementsOption')
        .addEventListener('click', showRemibursements);
    document.getElementById('submitReimbursmentOption')
        .addEventListener('click', showReimbursementSubmit);
    document.getElementById('reimbursement-submit')
        .addEventListener('click', submitReimbursement);
    document.getElementById('viewInfo')
        .addEventListener('click', showMyInformation);*/
})()
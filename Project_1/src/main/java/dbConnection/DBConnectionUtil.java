package dbConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnectionUtil {
	private static String url = "jdbc:postgresql://class-1809-jose.cbdtn23wkjhh.us-east-2.rds.amazonaws.com:5432/dbClass1809";
	private static String username = "jcontreras1";
	private static String password = "AWSFre3Tier!";
	
	static {
        try {
            DriverManager.registerDriver(new org.postgresql.Driver());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

	
	public static Connection newConnection() throws SQLException {
		return DriverManager.getConnection(url, username, password);
	}
}

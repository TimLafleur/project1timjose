import Dao.ReimbursementDaoImpl;
import com.model.Reimbursement;
import Dao.EmployeeDao;
import com.model.Employee;

import java.util.List;

/**
 * Created by tim on 10/17/2018.
 */
public class main {
    public static void main(String[] args){
        ReimbursementDaoImpl reimDao = new ReimbursementDaoImpl();

        List<Reimbursement> reimList = reimDao.findAll();
        
        EmployeeDao employeeDao = new EmployeeDao();

        List<Employee> employeeList = employeeDao.findAll();

        System.out.println(reimList.size());
        System.out.println(employeeList.size());
    }
}

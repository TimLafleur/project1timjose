package com.model;

import java.time.LocalDateTime;

public class Reimbursement {
		
	int reimbursementNumber;
	int amount;
	String status;
	int employee_id;
	int manager_id;
	LocalDateTime reimDate;
		
	public Reimbursement(){
			this.reimbursementNumber = 0;
			this.amount = 0;
			this.status = "";
			LocalDateTime reimDate = null;
			
	}

	public Reimbursement(int reimbursementNumber, int amount, String status){

		this.reimbursementNumber = 0;
		this.amount = 0;
		this.status = status;
		LocalDateTime reimDate = null;

	}

	public int getManager_id() {
		return manager_id;
	}

	public void setManager_id(int manager_id) {
		this.manager_id = manager_id;
	}

	public int getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(int employee_id) {
		this.employee_id = employee_id;
	}

	public LocalDateTime getReimDate() {
			return reimDate;
		}
		public void setReimDate(LocalDateTime reimDate) {
		this.reimDate = reimDate;
	}
		public int getreimbursementNumber() {
			return reimbursementNumber;
		}

		public void setreimbursementNumber(int reimbursementNumber) {
			this.reimbursementNumber = reimbursementNumber;
		}

		public int getAmount() {
			return amount;
		}

		public void setAmount(int amount) {
			this.amount = amount;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}
}

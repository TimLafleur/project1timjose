package com.model;

import java.util.ArrayList;
import java.util.List;

public class Employee {

	List<Employee> employeeList = new ArrayList();
	List <Reimbursement>reimbursementList = new ArrayList();

	int employeeNumber;
	String firstName;
	String lastName;
	String userName;
	String userPwd;
	String email;
	String employeeType;
		
		public Employee(){
			this.firstName = "";
			this.lastName = "";
			this.userName = "";
			this.userPwd = "";
			this.email = "";
			
		}
		
	public Employee(int employeeNumber, String firstName, String lastName, String userName, String userPwd, String email){
			
			this.employeeNumber = employeeNumber;
			this.firstName = firstName;
			this.lastName = lastName;
			this.userName  = userName;
			this.userPwd = userPwd;
			this.email = email;
			
		}

	public String getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}

	public int getEmployeeNumber() {
			return employeeNumber;
		}

		public void setEmployeeNumber(int employeeNumber) {
			this.employeeNumber = employeeNumber;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getUserName() {
			return userName;
		}

		public void setUserName(String userName) {
			this.userName = userName;
		}

		public String getUserPwd() {
			return userPwd;
		}

		public void setUserPwd(String userPwd) {
			this.userPwd = userPwd;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}
}

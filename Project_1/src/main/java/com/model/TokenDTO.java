package com.model;

import java.util.Collection;

public class TokenDTO {
    private String idToken;
    private String employee;
    private Collection roles;
    private String employeeType;

    public TokenDTO() {
    }

    public String getEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(String employeeType) {
        this.employeeType = employeeType;
    }

    public String getIdToken() {
        return idToken;
    }

    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }

    public String getUser() {
        return employee;
    }

    public void setUser(String user) {
        this.employee = user;
    }

    public Collection getRoles() {
        return roles;
    }

    public void setRoles(Collection roles) {
        this.roles = roles;
    }
}

package com.model;

import java.util.List;

public class Server {
    static List<Employee> allEmployees;
    static Employee selectedEmpoyee;
    static Employee managerSelected;
    static List<Reimbursement> allReimbursements;
    static List<Reimbursement> selectedReimbursements;

    private Server(){}

    public static void refineReimbursementSelection(String status, int employee_id) {
        if (employee_id < 0) {
            for (Reimbursement r : allReimbursements) {
                if (r.status == status) {
                    selectedReimbursements.add(r);
                }
            }
        } else {
            if(status == null){
                for (Reimbursement r : allReimbursements) {
                    if (r.employee_id == employee_id) {
                        selectedReimbursements.add(r);
                    }
                }
            } else {
                for (Reimbursement r : allReimbursements) {
                    if (r.status.equals(status) && r.employee_id == employee_id) {
                        selectedReimbursements.add(r);
                    }
                }
            }
        }
        return;
    }

    public static Employee getManagerSelected() {
        return managerSelected;
    }

    public static void setManagerSelected(Employee managerSelected) {
        Server.managerSelected = managerSelected;
    }

    public static Employee getEmployee(String userName){
        for(Employee e : getAllEmployees()){
            if(e.getUserName().equals(userName)) return e;
        }
        return null;
    }
    public static List<Employee> getAllEmployees() {
        return allEmployees;
    }

    public static void setAllEmployees(List<Employee> allEmployees) {
        Server.allEmployees = allEmployees;
    }

    public static Employee getSelectedEmpoyee() {
        return selectedEmpoyee;
    }

    public static void setSelectedEmpoyee(Employee selectedEmpoyee) {
        Server.selectedEmpoyee = selectedEmpoyee;
    }

    public static List<Reimbursement> getAllReimbursements() {
        return allReimbursements;
    }

    public static void setAllReimbursements(List<Reimbursement> allReimbursements) {
        Server.allReimbursements = allReimbursements;
    }

    public static List<Reimbursement> getSelectedReimbursements() {
        return selectedReimbursements;
    }

    public static void setSelectedReimbursements(List<Reimbursement> selectedReimbursements) {
        Server.selectedReimbursements = selectedReimbursements;
    }
}

package Dao;

import java.util.List;
import java.io.Serializable;
/**
 * Created by tim on 10/16/2018.
 */
public interface Dao<T> {
	
	 T findOne(String userName);
     List<T> findAll();
}

package Dao;

import com.model.Reimbursement;
import dbConnection.DBConnectionUtil;

import java.io.Serializable;
import java.sql.*;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;


/**
 * Created by tim on 10/17/2018.
 */
public class ReimbursementDaoImpl implements Dao<Reimbursement> {
    @Override
    public Reimbursement findOne(String id) {
        Connection c = null;
        Reimbursement reimbursement = null;

        try {
            c = DBConnectionUtil.newConnection();
            c.setAutoCommit(false);
            String sql = "SELECT EXISTS(SELECT 1 FROM reimbursement WHERE reim_number=?)";
            PreparedStatement s = c.prepareStatement(sql);
           // s.setInt(1, (int)id);

            ResultSet rs = s.executeQuery();
            if(!rs.next()){
                c.setAutoCommit(true);
                c.close();
                System.out.println("reimburement number " + id + " does not exist in database");
                return reimbursement;
            }
            c.close();

        } catch(SQLException e){
            if(c != null) {
                try {
                    c.close();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            e.printStackTrace();
        }

        try {
            c = DBConnectionUtil.newConnection();
            c.setAutoCommit(false);
            String sql = "SELECT reim_number, amount, status, date FROM reimbursement WHERE reim_number = ? LIMIT 1";
            PreparedStatement s = c.prepareStatement(sql);
            //s.setInt(1, (int)id);

            ResultSet rs = s.executeQuery();
            while(rs.next()){
                reimbursement = new Reimbursement();
                reimbursement.setAmount(rs.getInt("amount"));
                reimbursement.setStatus(rs.getString("status"));
                reimbursement.setreimbursementNumber(rs.getInt("reim_number"));
                reimbursement.setReimDate(LocalDateTime.ofInstant(rs.getDate("date")
                        .toInstant(), TimeZone.getTimeZone("America/New_York").toZoneId()));
            }
            c.commit();
            c.setAutoCommit(true);
            return reimbursement;
        }catch (SQLException e){
            e.printStackTrace();
            if(c != null){
                try {
                    c.rollback();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        }finally {
            try {
                c.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (c != null){
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return reimbursement;
    }

    public void UpdateReimbursement(Reimbursement r){

    }
    public void InsertReimbursement(Reimbursement r){
        System.out.println("we got here successful " + r.getEmployee_id());
        Connection c = null;

        try {
            c = DBConnectionUtil.newConnection();
            c.setAutoCommit(false);

            String sql = "INSERT INTO reimbursement (amount,status, employee_id) VALUES (?, ?, ?)";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, r.getAmount());
            ps.setString(2, r.getStatus());
            ps.setInt(3, r.getEmployee_id());

            //execute
            ps.executeUpdate();

            c.commit();

        } catch(SQLException ex) {
            ex.printStackTrace();
            if(c != null) {
                try {
                    c.rollback();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        } finally {
            if(c != null) {
                try {
                    c.setAutoCommit(true);
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public List<Reimbursement> findAllWhere(int id)
    {
        Connection c = null;
        List<Reimbursement> reimbursements = null;

        try {
            c = DBConnectionUtil.newConnection();
            c.setAutoCommit(false);
            String sql = "SELECT EXISTS(SELECT 1 FROM employee WHERE employee_number = ?)";
            PreparedStatement s = c.prepareStatement(sql);
            s.setInt(1, id);
            ResultSet rs = s.executeQuery();
            if(!rs.next()){
                c.setAutoCommit(true);
                c.close();
                System.out.println("employee does not exist");
            }
            c.close();

        } catch(SQLException e){
            if(c != null) {
                try {
                    c.close();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            e.printStackTrace();
        }

        try {
            c = DBConnectionUtil.newConnection();
            c.setAutoCommit(false);
            String sql = "SELECT amount, status, reim_number, employee_id FROM reimbursement WHERE employee_id =?";
            PreparedStatement s = c.prepareStatement(sql);
            s.setInt(1, id);

            reimbursements = new ArrayList<Reimbursement>();
            ResultSet rs = s.executeQuery();
            while(rs.next()){
                Reimbursement reimbursement = new Reimbursement();
                reimbursement.setAmount(rs.getInt("amount"));
                reimbursement.setStatus(rs.getString("status"));
                reimbursement.setreimbursementNumber(rs.getInt("reim_number"));
                reimbursement.setEmployee_id(rs.getInt("employee_id"));
                reimbursements.add(reimbursement);
            }
            c.commit();
            c.setAutoCommit(true);
            return reimbursements;

        }catch (SQLException e){
            e.printStackTrace();
            if(c != null){
                try {
                    c.rollback();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        }finally {
            try {
                c.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (c != null){
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return reimbursements;
    }

    @Override
    public List<Reimbursement> findAll() {
        Connection c = null;
        List<Reimbursement> reimbursements = null;

        try {
            c = DBConnectionUtil.newConnection();
            c.setAutoCommit(false);
            String sql = "SELECT EXISTS(SELECT 1 FROM reimbursement)";
            PreparedStatement s = c.prepareStatement(sql);
            ResultSet rs = s.executeQuery();
            if(!rs.next()){
                c.setAutoCommit(true);
                c.close();
                System.out.println("there are no reimbursement records");
            }
            c.close();

        } catch(SQLException e){
            if(c != null) {
                try {
                    c.close();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            e.printStackTrace();
        }

        try {
            c = DBConnectionUtil.newConnection();
            c.setAutoCommit(false);
            String sql = "SELECT amount, status, reim_number, employee_id FROM reimbursement";
            PreparedStatement s = c.prepareStatement(sql);

            reimbursements = new ArrayList<Reimbursement>();
            ResultSet rs = s.executeQuery();
            while(rs.next()){
                Reimbursement reimbursement = new Reimbursement();
                reimbursement.setAmount(rs.getInt("amount"));
                reimbursement.setStatus(rs.getString("status"));
                reimbursement.setreimbursementNumber(rs.getInt("reim_number"));
                reimbursement.setEmployee_id(rs.getInt("employee_id"));
                reimbursements.add(reimbursement);
            }
            c.commit();
            c.setAutoCommit(true);
            return reimbursements;

        }catch (SQLException e){
            e.printStackTrace();
            if(c != null){
                try {
                    c.rollback();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        }finally {
            try {
                c.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (c != null){
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return reimbursements;
    }

    public int approveReimbursement(int id) {
        Connection c = null;

        try {
            c = DBConnectionUtil.newConnection();
            c.setAutoCommit(false);
            String sql = "SELECT EXISTS(SELECT 1 FROM reimbursement WHERE reim_number=?)";
            PreparedStatement s = c.prepareStatement(sql);
            s.setInt(1, id);
            ResultSet rs = s.executeQuery();
            if(!rs.next()){
                c.setAutoCommit(true);
                c.close();
                System.out.println("there are no reimbursement records");
            }
            c.close();

        } catch(SQLException e){
            if(c != null) {
                try {
                    c.close();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            e.printStackTrace();
        }

        try {
            c = DBConnectionUtil.newConnection();
            c.setAutoCommit(false);
            String sql = "UPDATE reimbursement SET status=? WHERE reim_number =?";
            PreparedStatement s = c.prepareStatement(sql);
            s.setString(1, "approved");
            s.setInt(2, id);
            s.executeUpdate();

            c.commit();
            c.setAutoCommit(true);

        }catch (SQLException e){
            e.printStackTrace();
            if(c != null){
                try {
                    c.rollback();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        }finally {
            try {
                c.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (c != null){
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return id;
    }

    public int denyReimbursement(int id) {
        Connection c = null;

        try {
            c = DBConnectionUtil.newConnection();
            c.setAutoCommit(false);
            String sql = "SELECT EXISTS(SELECT 1 FROM reimbursement WHERE reim_number=?)";
            PreparedStatement s = c.prepareStatement(sql);
            s.setInt(1, id);
            ResultSet rs = s.executeQuery();
            if(!rs.next()){
                c.setAutoCommit(true);
                c.close();
                System.out.println("there are no reimbursement records");
            }
            c.close();

        } catch(SQLException e){
            if(c != null) {
                try {
                    c.close();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
            e.printStackTrace();
        }

        try {
            c = DBConnectionUtil.newConnection();
            c.setAutoCommit(false);
            String sql = "UPDATE reimbursement SET status=? WHERE reim_number =?";
            PreparedStatement s = c.prepareStatement(sql);
            s.setString(1, "deny");
            s.setInt(2, id);
            s.executeUpdate();

            c.commit();
            c.setAutoCommit(true);

        }catch (SQLException e){
            e.printStackTrace();
            if(c != null){
                try {
                    c.rollback();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        }finally {
            try {
                c.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (c != null){
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return id;
    }
}

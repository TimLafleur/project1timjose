package Dao;

import com.model.*;
import java.sql.*;

import dbConnection.DBConnectionUtil;

import java.util.ArrayList;
import java.util.List;

public class EmployeeDao implements Dao<Employee>{

	public void updateEmployeeInformation(Employee e){
		Connection c = null;

		try {
			c = DBConnectionUtil.newConnection();
			c.setAutoCommit(false);

			String sql = "UPDATE employee SET first_name=?," +
					"last_name=?, user_name=?,user_password=? WHERE employee_number = ?";
			PreparedStatement ps = c.prepareStatement(sql);
			ps.setString(1, e.getFirstName());
			ps.setString(2, e.getLastName());
			ps.setString(3, e.getUserName());
			ps.setString(4, e.getUserPwd());
			ps.setInt(5, e.getEmployeeNumber());


			//execute
			ps.executeUpdate();

			c.commit();

		} catch (SQLException ex) {
			ex.printStackTrace();
			if (c != null) {
				try {
					c.rollback();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		} finally {
			if (c != null) {
				try {
					c.setAutoCommit(true);
					c.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
	public Employee findOne(String userName)  {
		Connection c = null;
		Statement st = null;
		ResultSet rs = null;

		Employee temp = null;

		try {
			c = DBConnectionUtil.newConnection();
			c.setAutoCommit(false);
			String sql = "SELECT * FROM employee WHERE user_name = ?";
			PreparedStatement s = c.prepareStatement(sql);
			s.setString(1, userName);
			rs = s.executeQuery();

			while(rs.next()) {
				int employee_number = rs.getInt("employee_number");
				String first_name = rs.getString("first_name");
				String last_name = rs.getString("last_name");
				String user_name = rs.getString("user_name");
				String user_password = rs.getString("user_password");
				String email = rs.getString("email");

				temp = new Employee(employee_number, first_name, last_name, user_name, user_password, email);
				temp.setEmployeeType(rs.getString("employee_type"));
			}

			c.commit();
		} catch(SQLException ex) {
			ex.printStackTrace();
			if(c != null) {
				try {
					c.rollback();
				} catch(SQLException e) {
					e.printStackTrace();
				}
			}
		} finally {
			if(c != null) {
				try {
					c.setAutoCommit(true);
					c.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return temp;
	}

	public Employee findOne(int id)  {
		Connection c = null;
		Statement st = null;
		ResultSet rs = null;

		Employee temp = null;

		try {
			c = DBConnectionUtil.newConnection();
			c.setAutoCommit(false);
			String sql = "SELECT * FROM employee WHERE employee_number = ?";
			PreparedStatement s = c.prepareStatement(sql);
			s.setInt(1, id);
			rs = s.executeQuery();

			while(rs.next()) {
				int employee_number = rs.getInt("employee_number");
				String first_name = rs.getString("first_name");
				String last_name = rs.getString("last_name");
				String user_name = rs.getString("user_name");
				String user_password = rs.getString("user_password");
				String email = rs.getString("email");

				temp = new Employee(employee_number, first_name, last_name, user_name, user_password, email);
				temp.setEmployeeType(rs.getString("employee_type"));
			}

			c.commit();
		} catch(SQLException ex) {
			ex.printStackTrace();
			if(c != null) {
				try {
					c.rollback();
				} catch(SQLException e) {
					e.printStackTrace();
				}
			}
		} finally {
			if(c != null) {
				try {
					c.setAutoCommit(true);
					c.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return temp;
	}


	public List<Employee> findAll() {
		Connection c = null;
		Statement st = null;
		ResultSet rs = null;

		Employee temp;
		List <Employee>employeeResults = new ArrayList<>();

		try {
			c = DBConnectionUtil.newConnection();
			c.setAutoCommit(false);
			st = c.createStatement();
			rs = st.executeQuery("SELECT * FROM employee");
			while(rs.next()) {
				int employee_number = rs.getInt("employee_number");
				String first_name = rs.getString("first_name");
				String last_name = rs.getString("last_name");
				String user_name = rs.getString("user_name");
				String user_password = rs.getString("user_password");
				String email = rs.getString("email");

				temp = new Employee(employee_number, first_name, last_name, user_name, user_password, email);
				temp.setEmployeeType(rs.getString("employee_type"));
				employeeResults.add(temp);
			}

			c.commit();
		} catch(SQLException ex) {
			ex.printStackTrace();
			if(c != null) {
				try {
					c.rollback();
				} catch(SQLException e) {
					e.printStackTrace();
				}
			}
		} finally {
			if(c != null) {
				try {
					c.setAutoCommit(true);
					c.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		Server.setAllEmployees(employeeResults);
		return employeeResults;
	}
}
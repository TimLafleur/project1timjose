package web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.Employee;
import com.model.Server;
import com.model.TokenDTO;
import service.AuthenticationService;
import service.EmployeeService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class EmployeeServlet extends HttpServlet {
    EmployeeService service = new EmployeeService();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        System.out.println("Here we are in employee servlet");
        String option = req.getParameter("action");

        switch (option) {
            case "updateEmployeeInfo":
                Server.getSelectedEmpoyee().setFirstName(req.getParameter("firstName"));
                Server.getSelectedEmpoyee().setLastName(req.getParameter("lastName"));
                Server.getSelectedEmpoyee().setUserName(req.getParameter("email"));
                Server.getSelectedEmpoyee().setUserPwd(req.getParameter("password"));
                service.save(Server.getSelectedEmpoyee());
                break;
            case "getAllEmployees":
                List<Employee> employeeList = service.getAll();
                resp.getWriter().write(new ObjectMapper().writeValueAsString(employeeList));
                break;
            case "findOneEmployee":
                int employeeId = Integer.parseInt(req.getParameter("employeeId"));
                Employee employee = service.getOne(employeeId);
                Server.setManagerSelected(employee);
                resp.getWriter().write(new ObjectMapper().writeValueAsString(employee));
                break;
            default:
                break;
        }

    /*@Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPut(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doDelete(req, resp);
    }*/
    }
}

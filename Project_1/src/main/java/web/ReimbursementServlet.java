package web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.Reimbursement;
import com.model.Server;
import service.ReimbursementService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

public class ReimbursementServlet extends HttpServlet {
    ReimbursementService service = new ReimbursementService();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String option = req.getParameter("action");

        switch(option){
            case "insertReimbursement":
                Reimbursement r = new Reimbursement();
                r.setAmount(Integer.parseInt(req.getParameter("amount")));
                r.setReimDate(LocalDateTime.now());
                // the employee never has an employee number
                System.out.println(Server.getSelectedEmpoyee());

                r.setEmployee_id(Server.getSelectedEmpoyee().getEmployeeNumber());
                System.out.println(Server.getSelectedEmpoyee().getEmployeeNumber());
                r.setStatus("pending");
                service.save(r);
                break;

            case "getReimbursements":
                List<Reimbursement> reimList = service.getAll();
                resp.getWriter().write(new ObjectMapper().writeValueAsString(reimList));
                break;
            case "getReimbursementsWhere":
                List<Reimbursement> reimListWhere = service.getAllWhere(Server.getSelectedEmpoyee().getEmployeeNumber());
                resp.getWriter().write(new ObjectMapper().writeValueAsString(reimListWhere));
                break;
            default:
                break;

            case "approveReimbursement":
                service.approveReimbursement(Integer.parseInt(req.getParameter("reimbursement")));
                break;

            case "denyReimbursement":

                service.denyReimbursement(Integer.parseInt(req.getParameter("reimbursement")));
                break;
            case "getReimbursementsWhereManagerSelected":
                List<Reimbursement> reimListWhereManager = service.getAllWhere(Server.getManagerSelected().getEmployeeNumber());
                resp.getWriter().write(new ObjectMapper().writeValueAsString(reimListWhereManager));
                break;

        }




        return;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}

package web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.Employee;
import com.model.Reimbursement;
import com.model.Server;
import com.model.TokenDTO;
import service.AuthenticationService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

public class AuthServlet extends HttpServlet {
    AuthenticationService service = new AuthenticationService();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //get username and password
        Employee e = new ObjectMapper().readValue(req.getInputStream(), Employee.class);
        System.out.println(e.getUserName());
        System.out.println(req.getInputStream());
        if(e.getUserName() == null || e.getUserPwd() == null) {
            resp.setStatus(400);
            resp.getWriter().write("Username and Password are required");
            return;
        }

        //send authService
        e = service.authenticate(e);
        //if authenticated set session
        if(e != null) {
            try {
                TokenDTO token = service.getToken(e, 360000);
                resp.getWriter().write(new ObjectMapper().writeValueAsString(token));
                Server.setSelectedEmpoyee(e);
                System.out.println(e.getEmployeeNumber());
                resp.setStatus(200);
                return;
            } catch(Exception e1) {
                resp.setStatus(500);
                return;
            }
        } else {
            //if !autheticated send 401
            System.out.println("I'm here, not authenticated u == null from authenticate");
            resp.setStatus(401);
            return;
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String option = req.getParameter("action");

        switch(option){
            case "logout":
                Server.setSelectedEmpoyee(null);
                resp.sendRedirect("http://localhost:8080/login.html");
                resp.setStatus(300);
                break;
        }
    }
}

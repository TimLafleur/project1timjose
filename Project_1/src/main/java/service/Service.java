package service;

import java.io.Serializable;
import java.util.List;

public interface Service<T> {
	
	T getOne(String id);
    List<T> getAll();
    void save(T obj);
    void delete(T obj);
    void delete(Serializable employeeNumber);

}

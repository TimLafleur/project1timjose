package service;

import Dao.EmployeeDao;
import com.model.Employee;

import java.io.Serializable;
import java.util.List;

/**
 * Created by tim on 10/19/2018.
 */
public class EmployeeService implements Service<Employee> {
    EmployeeDao employeeDao = new EmployeeDao();

    @Override
    public Employee getOne(String id) {
        return employeeDao.findOne(id);
    }

    public Employee getOne(int id) {
        return employeeDao.findOne(id);
    }

    @Override
    public List<Employee> getAll() {
        return employeeDao.findAll();
    }

    @Override
    public void save(Employee obj) {
        employeeDao.updateEmployeeInformation(obj);
    }

    @Override
    public void delete(Employee obj) {

    }

    @Override
    public void delete(Serializable employeeNumber) {

    }
}

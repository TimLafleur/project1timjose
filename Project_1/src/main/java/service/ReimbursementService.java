package service;

import Dao.ReimbursementDaoImpl;
import com.model.Reimbursement;

import java.io.Serializable;
import java.util.List;

/**
 * Created by tim on 10/19/2018.
 */
public class ReimbursementService implements Service<Reimbursement> {
    ReimbursementDaoImpl reimbursementDaoImpl = new ReimbursementDaoImpl();

    @Override
    public Reimbursement getOne(String id) {
        return reimbursementDaoImpl.findOne(id);
    }

    @Override
    public List<Reimbursement> getAll() {
        return reimbursementDaoImpl.findAll();
    }

    public List<Reimbursement> getAllWhere(int employeeId) {
        return reimbursementDaoImpl.findAllWhere(employeeId);
    }

    @Override
    public void save(Reimbursement obj) {
        reimbursementDaoImpl.InsertReimbursement(obj);
    }

    @Override
    public void delete(Reimbursement obj) {

    }

    @Override
    public void delete(Serializable employeeNumber) {

    }

    public void approveReimbursement(int id){
        reimbursementDaoImpl.approveReimbursement(id);
    }

    public void denyReimbursement(int id){
        reimbursementDaoImpl.denyReimbursement(id);
    }
}

package service;

import com.model.Employee;
import com.model.TokenDTO;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.Jwts;

import java.io.UnsupportedEncodingException;
import java.time.Instant;
import java.util.Date;

public class AuthenticationService {

    EmployeeService employeeService = new EmployeeService();

    public Employee authenticate(Employee e){
        Employee in = employeeService.getOne(e.getUserName());

        if(in != null) {
            if(e.getUserName().equals(in.getUserName())) {
                if(in.getUserPwd().equals(e.getUserPwd())) {
                    return in;
                }
            }

        }
        return null;
    }

    public TokenDTO getToken(Employee e, long ttl) throws UnsupportedEncodingException {
        TokenDTO token = new TokenDTO();
        Date now = new Date();
        Date future = Date.from(Instant.ofEpochMilli(now.getTime() + ttl));

        String jwt = Jwts.builder()
                .setSubject(e.getUserName())
                .setIssuedAt(now)
                .setExpiration(future)
                .claim("employee", e.getUserName())
                .claim("type", e.getEmployeeType())
                .signWith(SignatureAlgorithm.HS256, "mySecret".getBytes("UTF-8"))
                .compact();

        token.setIdToken(jwt);
        token.setUser(e.getUserName());
        token.setEmployeeType(e.getEmployeeType());

        return token;
    }
}

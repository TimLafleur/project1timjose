(function(){
    const getLogin = () => {
        location.href = "http://localhost:8080/login.html";
    }

    if(!localStorage.getItem('profile')) {
        getLogin();
    }

    fetch('http://localhost:8080/games', {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer ' + localStorage.getItem('profile')
        }
    })
        .then(resp => resp.json())
        .then(json => console.log(json));

    document.querySelector('#logout')
        .addEventListener('click', (evt) => {
            evt.preventDefault();
            localStorage.removeItem('profile');
            localStorage.removeItem('username');
            localStorage.removeItem('roles');
            location.href = "http://localhost:8080/login.html";
        });
})()